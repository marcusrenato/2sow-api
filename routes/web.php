<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('cidadao', 'CitizenController@index');
$router->post('cidadao', 'CitizenController@store');
$router->get('cidadao/{cpf}', 'CitizenController@show');
$router->put('cidadao/{id}', 'CitizenController@update');
$router->delete('cidadao/{id}', 'CitizenController@destroy');

$router->get('cep/{cep}', 'ZipCodeController@verifyZipCode');