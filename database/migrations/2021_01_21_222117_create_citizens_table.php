<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitizensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizens', function (Blueprint $table) {
            $table->id();

            $table->string('nome', 50);
            $table->string('sobrenome', 50);
            $table->string('cpf', 18)->unique();
            $table->string('email', 50);
            $table->string('celular', 25);
            $table->string('cep', 12);
            $table->string('logradouro', 100);
            $table->string('bairro', 25);
            $table->string('cidade', 25);
            $table->string('uf', 25);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizens');
    }
}
