# API 2 Sow

### Instruções

- Versão PHP: 7.3 
- Banco: Mysql
- Ao baixar instalar pacotes: ``composer install``. 
- Copiar env: ``cp .env.exampe .env`` e alterar as informações do banco que você criar. 
- Executar migrations: ``php artisan migrate`` 
- Iniciar Projeto: ``php -S localhost:8080 -t public`` 

### Explicações
A documentação da API encontra-se na raíz do projeto dentro da pasta DOCS. Caso queriam ver visualmente, sugiro instalar um server do npm (__https://github.com/vercel/serve__) e executar o comando dentro da pasta. Ou abrir o arquivo ``doc.json`` no insomnia. 
Não pude adicionar a possibilidade de uso pela linha de comando. Tambḿe não utilizei testes ou docker, por estar me aprofundando nestes assuntos atualmente.
Optei por densenvolver o teste em Lumen pela maior rapidez que o framework me proporciona a desenvolver. Gostaria de agradecer a oportunidade, caso se interessem, posso também mostrar trabalhos que desenvolvo na empresa que trabalho atualmente, para que conheçam minhas habilidades. 