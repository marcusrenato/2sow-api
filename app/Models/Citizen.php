<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
    protected $fillable = [
        'nome',
        'sobrenome',
        'cpf',
        'email',
        'celular',
        'cep',
        'logradouro',
        'bairro',
        'cidade',
        'uf'
    ];
}