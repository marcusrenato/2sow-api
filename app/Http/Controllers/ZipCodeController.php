<?php

namespace App\Http\Controllers;

class ZipCodeController extends Controller
{
    /**
     * Get zip code informations
     *
     * @param string $cep
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyZipCode(string $cep)
    {
        if (strlen($cep) >= 8 && strlen($cep) <= 9) {
            $url = 'https://viacep.com.br/ws/' . $cep . '/json/';

            $info = json_decode(file_get_contents($url));

            return response()->json([
                'data' => $info
            ]);
        }

        abort(400);
    }
}
