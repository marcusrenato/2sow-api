<?php

namespace App\Http\Controllers;

use App\Http\Resources\CitizenResource;
use App\Models\Citizen;
use Illuminate\Http\Request;

class CitizenController extends Controller
{
    /**
     * The citizen instance
     *
     * @var Citizen
     */
    protected $citizen;

    /**
     * Create a new controller instance.
     *
     * @param Citizen $citizen
     * @return void
     */
    public function __construct(Citizen $citizen)
    {
        $this->citizen = $citizen;
    }

    /**
     * Return all citizens
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $citizens = $this->citizen->orderBy('nome')->get();

        return response()->json([
            'data' => CitizenResource::collection($citizens)
        ]);
    }

    /**
     * Add to new citizen
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $this->validator($request);

        try {
            $citizen = $this->citizen->create($data);

            return response()->json([
                'data' => new CitizenResource($citizen)
            ], 201);
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            abort(500);
        }
    }

    /**
     * Search citizen by cpf
     *
     * @param string $cpf
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $cpf)
    {
        $citizen = $this->citizen
            ->where('cpf', $cpf)
            ->first() ?? abort(404);

        return response()->json([
            'data' => new CitizenResource($citizen)
        ]);
    }

    /**
     * Update a specific citizen
     *
     * @param integer $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, Request $request)
    {
        $data = $this->validator($request, $id);
        $citizen = $this->citizen->findOrFail($id);

        try {
            $citizen->update($data);

            return response()->json([
                'data' => new CitizenResource($citizen)
            ]);
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            abort(500);
        }
    }

    /**
     * Delete a specific citizen
     *
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $citizen = $this->citizen->findOrFail($id);

        try {
            $citizen->delete();

            return response()->json([], 204);
        } catch (\Throwable $th) {

            if (env('APP_DEBUG')) {
                dd($th->getMessage());
            }

            abort(500);
        }
    }

    /**
     * Check if data is valid for insertion
     *
     * @param Request $request
     * @param integer $id
     * @return array
     */
    public function validator(Request $request, int $id = null): array
    {
        $rules = [
            'nome'          => 'required|string|max:50|min:3',
            'sobrenome'     => 'required|string|max:50|min:3',
            'cpf'           => 'required|max:18|min:9|unique:citizens,cpf',
            'email'         => 'required|email|max:25|min:3',
            'celular'       => 'required|string|max:25|min:8',
            'cep'           => 'required|string|max:12|min:8',
            'logradouro'    => 'required|string|max:100|min:3',
            'bairro'        => 'required|string|max:25|min:3',
            'cidade'        => 'required|string|max:25|min:3',
            'uf'            => 'required|string|max:25|min:3'
        ];

        if (!is_null($id)) {
            $rules['cpf'] = "required|max:18|min:9|unique:citizens,cpf,{$id},id";
        }

        return $this->validate($request, $rules);
    }
}
